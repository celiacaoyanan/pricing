package com.citi.trading.pricing;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;


public class MyClassTest {
	
	private MyClass target;
	
	@Before
	public void setup() {
		target=new MyClass(4);
	}
	
	@Test
	public void testGetSquare() {
		//MyClass target=new MyClass(6);
		//assertEquals(16,target.getSquare());
		assertThat(target.getSquare(),equalTo(16));
	}
	
	@Test
	public void testGetSquareRoot() {
		assertThat(target.getSquareRoot(),closeTo(2.0,0.0001));
	}

}
